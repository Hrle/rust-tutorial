pub fn add_two(a: i32, b: i32) -> i32 {
  a + b
}

#[test]
fn test_add_two() {
  assert_eq!(add_two(0, 1), 1);
}

#[test]
fn test_add_two_result() -> Result<(), String> {
  if add_two(0, 1) == 1 {
    Ok(())
  }
  else {
    Err(String::from("yes"))
  }
}
