#[allow(dead_code)]
pub fn functions() {
  let a = ();
  println!("{:?}", a);

  let b = { 1 };
  println!("{}", b);
}
