#[allow(dead_code)]

pub fn arithmetic() {
  let a = 1;
  let b = 2;
  let c = a as f32 / b as f32;
  println!("{}", c);

  let c = 1 / 2_i8;
  println!("{}", c);
}
