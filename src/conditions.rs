use crate::input::get_line;

#[allow(dead_code)]
pub fn conditions() {
  if true {
    println!("this is an if");
  }

  let str = "ayy";
  if str == "ayy" {
    println!("string equality is sane!");
  }

  println!("Do you want pizza?");
  let input = get_line();
  if input == "yes" {
    println!("Here you go!");
  } else if input == "no" {
    println!("You shan't get any pizza!");
  } else {
    println!("Make up your mind!");
  }
}
