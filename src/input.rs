use std::io;

pub fn get_line() -> String {
  let mut input = String::new();
  // NOTE: &mut mutable reference kinda like just & in C++
  io::stdin()
    .read_line(&mut input)
    .expect("failed to read input");
  input.trim().to_string()
}

#[allow(dead_code)]
pub fn input() {
  println!("Enter input:");
  let input = get_line();

  let x: u32 = input.trim().parse().unwrap_or(0);
  println!("{}", x + 1);
}
