#[allow(dead_code)]
pub fn enums() {
  #[derive(Debug)]
  enum IpAddressKind {
    V4(String),
    V6(u8, u8, u8, u8),
    V8 { address: String },
    V10(Box<IpAddressKind>),
  }

  dbg!(IpAddressKind::V4(String::from("127.0.0.1")));
  dbg!(IpAddressKind::V6(127, 0, 0, 1));
  dbg!(IpAddressKind::V8 {
    address: String::from("127.0.0.1")
  });

  dbg!(match IpAddressKind::V6(127, 0, 0, 1) {
    IpAddressKind::V6(_, _, _, _) => 1,
    _ => 2,
  });
}
