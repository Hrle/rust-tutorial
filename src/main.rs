pub mod arithmetic;
pub mod conditions;
pub mod enums;
pub mod functions;
pub mod input;
pub mod structs;
pub mod variables;

#[cfg(test)]
pub mod tests;

fn main() {
  // variables::variables();
  // input::input();
  // arithmetic::arithmetic();
  // conditions::conditions();
  // functions::functions();
  // structs::structs();
  enums::enums();
}
