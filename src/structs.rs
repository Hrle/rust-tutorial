#[allow(dead_code)]
pub fn structs() {
  #[derive(Debug)]
  struct MyStruct {
    my_field: i32,
  }

  impl MyStruct {
    fn my_method(&self) -> i32 {
      self.my_field
    }

    fn my_static_method() -> i32 {
      0
    }
  }

  impl MyStruct {
    fn my_other_method(self) -> i32 {
      self.my_field
    }
  }

  let my_instance = MyStruct { my_field: 1 };
  println!("{:#?}", my_instance);
  dbg!(my_instance.my_method());
  dbg!(my_instance.my_other_method());
  dbg!(MyStruct::my_static_method());

  #[derive(Debug)]
  struct MyTupleStruct(i32);
  let my_tuple_instance = MyTupleStruct(1);
  dbg!(&my_tuple_instance);
  dbg!(my_tuple_instance);
  dbg!(MyTupleStruct(2));

  #[derive(Debug)]
  struct MyUnitStruct;
  let my_unit_instance = MyUnitStruct;
  println!("{:?}", my_unit_instance);
}
