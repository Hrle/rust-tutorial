#[allow(dead_code)]
pub fn variables() {
  let mut x: f64 = 4.0;
  println!("hello world! {}", x);
  x = 5.2;
  println!("hello world! {}", x);
  let x = 3;
  println!("hello world! {}", x);
  let t = (1, 2, 3);
  let (y, _, _) = t;
  println!("hello world! {} {}", y, t.2);
  println!("hello world! {:?}", t);
  let x = x as u8;
  println!("hello world! {}", x);
}
