# Rust tutorial

First steps into Rust.

## Variables

- has type inference with `let`
- implicitly immutable - `mut` after `let` for mutable
- you can redefine immutable/mutable variables - known as `shadowing`
- has scopes like `c`
- there are constants which are different from immutable variables and they're
  written in uppercase

## Types

- specified like in `typescript`
- can choose number of bits and signed/unsigned for integers
  (`i32`, `u32`, ...)
- floats are `f32`, doubles are `f64`
- boolean and characters are like `c`
- tuples can be `destructured`, indexed with `.<index>`, printed with debug
  print (`{:?}`), and types are `(<type1>, <type2>, ...)`
- arrays are pretty much like you would expect at this point except that the
  type is `[<type_of_element>; <number_of_elements>]`
- there is also the unit (`()`) type which is kinda like `void` but much more
  usable (it has a hash, order, equality...) and is the default type returned
  from functions that don't return anything

## Casting

- either with `as` or just appending the type on literals like `2_i32`
- you can put `_` anywhere inside number literals like `2_000`

## Modules

- use modules in main with `mod`
- use modules in current tree with `use crate::<module>::<member>`
- use external modules with `use`

## Input

- look at `input.rs`

## Arithmetic

- detects overflows
- disallows arithmetic with types that are not the same
- to do any kind of arithmetic with types that are not the same you have to
  cast values into the appropriate type with `as`
- arithmetic operators are `+`, `-`, `*`, `/`, and `%`

## Conditions

- conditional operators are the same as in `c` (comparison and logical)
- same as arithmetic, conditions with types that are not the same is not
  allowed
- string equality is actually sane!!!

## Functions

- with `fn` and `return`
- return types are declared kinda like the new `c++` notation
- you can omit the `return` keyword if you omit the semicolon

## Statements

- in example, `let`

## Expressions

- in example, addition (`+`)
- can be expressed by creating a scope (`{}`) and returning a value from it by
  just typing in the last expression in the scope without a semicolon

## Ownership

- uses a very similar memory model to `c++` if you take into account its
  mutability constraints and ownership constraints
- `RAII` and ownership semantics are baked into the language, unlike `c++`
  where `RAII` is baked into the language, but ownership semantics are to be
  implemented by libraries
- the `drop` function is used like a `deconstructor` in `c++` and is provided by
  the `Drop` trait
- with immutable variables, rust implicitly makes moves on `lvalues` rather
  then copies like in `c++`
- the `clone` functions is used instead of copy assignment/construction in
  `c++`
- there is no difference between moving and copying trivial types, that is,
  trivial types are always copied
- `drop` is provided by the `Drop` trait and is used for non-trivial types
- the `Clone` trait is used for trivial types
- you cannot implement both the `Drop` and `Copy` trait
- passing an argument is the same in terms of memory as assigning a variable,
  that is, the function takes ownership of non-trivial types, or in other
  words, passing an argument moves the value for non-trivial types
- function give ownership of return values to the variable/argument in which
  the return value is stored for non-trivial types, that is, return values are
  moved onto the variable/argument in which they are stored
- values can be used without passing ownership using references
- you can get a reference to any value with the `&` operator kind of like
  pointers in `c++`
- you can `dereferece` references with `*` same as pointers in `c++`
- references can be mutable and immutable
- you cannot have more than one mutable reference to a value in a single scope
  because of thread safety guarantees
- you cannot have an mutable reference and an immutable one in the same scope
  because that would also violate thread safety guarantees
- `rust` creates scopes on it's own when usages of mutable and immutable
  references don't overlap, so you can have mutable and immutable references in
  the same scope as long as you don't mix their usage
- slices are used to reference parts of contiguous memory which are kind of
  like slices in `python`, but unlike `python`, you cannot use negative values
  and there is no guarantee that a slice is occurring, for example, on
  a character boundary of a string, in other words, it is not an operator like
  in `python` that guarantees "alignment", but more like a literal part of
  a sequence of bytes
- for strings, string literals are slices
- for arrays, references to the array are slices
- slices to a value are counted as immutable references to a value, so you
  can't have a mutable reference and a slice of a value in the same scope

## Structs

- pretty much what you would expect
- there are also things similar to `javascript` like omitting a field name if
  the variable that contains the field value is named the same as the field and
  object spreading if the type of the object is the same as the structs type
- structs can be declared inside functions
- you can make it debug-printable (`{:?}`) with `#[derive(Debug)]`
- tuple structs are type aliases for tuples
- unit structs are structs with no members and behave similarly to the unit
  type (`()`)
- use `{:#?}` in `println!` for prettier debug output
- `dbg!` prints to `stderr` the variable name, file, and line where the
  variable is located
- `methods` can be defined anywhere and the first argument is a
  mutable/immutable reference or value of `self` like in `python` which means
  you can either take ownership of `self` inside the method or borrow it
  mutably or immutably
- inside `impl` the `Self` type is an alias for the type for which methods are
  defined
- instead of `.` and `->` it has automatic referencing/de-referencing!!
- functions in `impl` blocks are called associated functions, duh...
- static methods just don't take a `self` argument and are usually used for
  constructors
- struct members are private by default

## Enums

- they're actually runtime-safe unions!
- `#[derive(Debug)]` for debug print
- you can create methods on enums with `impl` the same way you would do with
  structs
- `Option` is the enum that introduces nullability!!!
- `match` is used to do different things depending on the variant of an enum
  and is similar to `c#` pattern matching but even better
- `match` must be exhaustive
- there is also `if let` which is like pattern matching inside `if` in `c#`

## Packages

- a `package` is one or more `crates`
- packages can have multiple `binary crates` and at most one `library crate`
- `crate root` is the source `rust` starts from for any `crate` and is either
  `mian.rs` for `binary crates` or `lib.rs` for `library crates`
- a `crate` can be both a `binary crate` and a `library crate` at the same time
  by having `src/main.rs` and a `src/lib.rs`
- `modules` are basically namespaces that can either be declared inline
  (`mod <name> { ... }`), in files like `src/**/<name>.rs` or in files like
  `src/**/<name>/mod.rs`
- members of `modules` which includes `module` members are private by default
  and `pub` is used to make them public for consumption with `mod <name>;`
- `pub` refers to whether a member is visible to parent or sibling `modules`
  and can be placed on `modules`, `functions`, `enums`, `structs`, and `fields`
- the `use` keyword brings members of other modules into current scope
- think of `mod` as export `pub` as export to other `crates` and `use` as
  import
- `crate::` is resolution by absolute path based on the current `crate`, `self`
  is resolution by relative path based on the current `module`, and `super` is
  resolution by relative path based on the parent of the current `module`
- `use` is used idiomatically when `functions` imported with imports to their
  parent module and `structs` and `enums` are imported with imports to the
  `struct` or `enum` except when those `structs` or `enums` have a conflicting
  name in which case they are imported with imports to their parent modules
- `as` creates an alias to an import
- `pub` on `use` has the effect of re-exporting
- `{<import1>, <import2>, ...}` in `use` can import multiple members, `self`
  inside `{}` imports the module preceding the `{}`, and `*` in `{}` imports
  all public members in the module preceding `{}`
- `src/<module_name>.rs` is conventional over `src/<module_name>/mod.rs`

## Collections

- `vectors`, `strings`, and `hash maps`
- `vec!` to instantiate a vector with elements
- `str` is in ASCII bytes, `String` is a UTF-8 byte `Vector`
- `.chars()` and `.bytes()` to iterate over strings
- `format!` is like in `python`
- indexing on strings is not supported because of encoding

## Errors

- `recoverable` with `Result` and `unrecoverable` with `panic!`
- `match` is pretty much what you would expect
- `unwrap_or_else` uses closures
- `unwrap` `panic!`s
- `expect` `panic!`s with a message
- `propagate` errors by returning them
- `?` to bypass propagation
- `?.` not only executes the rest when an error happens but also `propagate`s
  it!!
- `panic!` means that the developer is wrong and `Result` means that the
  user is wrong

## Generics

- syntax similar to `typescript`
- constraints same as `c++` except `trait`s are nominal
- `impl<...>` for functions over generic types - can be generic or only for
  a specific generic instantiation
- in terms of performance, generics are basically templates to the compiler
- `impl ... with ...` to implement trait methods on types
- restriction on implementations is that you can't implement traits on types
  where the trait and the type are external
- there are default implementations of traits like in `c#` interfaces
- traits can be parameters like in `c++` and you use `impl` instead of `auto`
- traits as parameters is syntax sugar for actual generics like in `c++`
- `trait bounds` are constraints
- combine traits in `trait bounds` with `+`
- use `where` like in `c#`
- returning traits from functions is possible
- methods can be implemented conditionally by specifying the bounds of types in
  `impl<...>`
- lifetime parameters tell to rust that the references with a particular
  lifetime parameter must live at least as long as that lifetime parameter
- putting lifetime parameters in structs is also possible
- lifetime can be elided if the compiler knows what lifetime is used where
- `'static` denotes static lifetime duh...

## Testing

- with `test` macro
- use `asserts_*` for assertions
- fail tests with `panic!`
- custom messages after assertion arguments
- `should_panic`
- return `Result` for a testing model without `panic!`
- `cargo test` is parallel by default
- `--test-threads` parameter for setting number of test threads
- `--show-output` parameter for showing output of passing tests
- add name of test and it `fuzzy` finds the tests you want to run
- `--ignored` to run ignored tests
- `--include-ignored` to run all tests
- `#[cfg(test)]` to mark a module as only compiled for tests
- unit tests in `src` and integration tests in `tests`

## `CLI`

- `env::args().collect()` for arguments
- `env::var()` for environment variables
- `eprintln!` to standard error
